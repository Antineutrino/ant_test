import numpy as np
import time

def path_length(path):
    length = 0
    for i in path:
        length += distances[i]
    return length


def calc_paths():
    all_paths = []
    for i in range(num_ants):
        start = 0
        path = []
        visited = set()  # вершины графа не повторяются
        visited.add(start)
        prev = start
        for i in range(len(distances) - 1):
            # print(pheromone)
            move = select_move(pheromone[prev], distances[prev], visited)
            path.append((prev, move))  # записываем рёбра в привычном формате
            prev = move
            visited.add(move)
        path.append((prev, start))
        all_paths.append((path, path_length(path)))
    return all_paths


def select_move(pheromone, dist, visited):  # выбор хода
    pheromone = np.copy(pheromone)
    pheromone[list(visited)] = 0  # обнуляем феромон там, где были
    p1 = pheromone ** alpha * ((1.0 / dist) ** beta)
    p = p1 / p1.sum()
    move = np.random.choice(range(dim), 1, p=p)[0]  # выбор хода по вероятности p
    return move



# часть кода для степика
# sp = []
# s = input().split()
# l = len(s)
# sp.append(s)
# for i in range(l-1):
#     s = input().split()
#     sp.append(s)
#
# d = []
# for i in sp:
#     if len(i) > 0:
#         d.append([int(x) for x in i.split()])
#
# l = len(d)  # тут считаем размерность происходящего
# for i in range(l):
#     if d[i][i] == 0:
#         d[i][i] = np.inf
#
# distances = np.asarray(d)
# конец части получения матрицы от степика


np.random.seed(7)
distances = np.random.randint(1, 50, size=(20, 20))

a = time.time()  # отслеживаем время работы алгоритма

dim = len(distances)
pheromone = np.ones((dim, dim)) / dim  # начальная матрица феромонов

num_ants = 10  # 20 - выигрышное в тестах количество
iterations = 100  # 300 - выигрышное в тестах количество
evaporation = 0.8  #
alpha = 1  #
beta = 4  #

result_path = ("path", np.inf)  # нулевой бесконечный путь для сравнения
for i in range(iterations):
    # print('Основной цикл по итерациям')  # отладочное
    all_paths = calc_paths()  # вычисляем весь путь
    # print(all_paths)
    # Здесь должно быть добавление феромона
    sorted_paths = sorted(all_paths, key=lambda x: x[1])
    for path, dist in sorted_paths:
        for move in path:
            pheromone[move] += 1.0 / distances[move]  # добавление феромона
    short_path = min(all_paths, key=lambda x: x[1])
    # print(short_path)  # отладочное
    if short_path[1] < result_path[1]:
        result_path = short_path
    pheromone = pheromone * evaporation

b = time.time()
print(int(result_path[1]))
print(b - a)  # время в секундах
